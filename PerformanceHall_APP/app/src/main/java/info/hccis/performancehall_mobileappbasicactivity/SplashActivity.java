package info.hccis.performancehall_mobileappbasicactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Sleeping for a bit to let user usee the splash screen.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //startActivity(new Intent(SplashActivity.this, GoogleSignInActivity.class));
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }
}


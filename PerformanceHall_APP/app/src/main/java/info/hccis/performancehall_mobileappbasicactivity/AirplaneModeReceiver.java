package info.hccis.performancehall_mobileappbasicactivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AirplaneModeReceiver extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent){

        boolean isAirplaneModeEnabled = intent.getBooleanExtra("state",false);

        if(isAirplaneModeEnabled){
            Toast.makeText(context,"Airplane Mode Enabled", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(context,"Airplane Mode Disbled", Toast.LENGTH_LONG).show();
        }
    }
}

# README #



### What is this repository for? ###

This repo is a contains a brief introduction to Broadcast Receivers. Contains examples coded in Java and Kotlin.


### Contribution guidelines ###

Video reference - https://www.youtube.com/watch?v=lldf3nei2rQ
Other sources - Android Broadcast Receivers - TechVidvan, https://medium.com/android-news/9-ways-to-avoid-memory-leaks-in-android-b6d81648e35e, https://www.tutorialspoint.com/android/android_broadcast_receivers.htm

### Who do I talk to? ###

Jeff Thistle
jthistle@hollandcollege.com
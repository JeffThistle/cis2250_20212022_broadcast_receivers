package info.hccis.broadcastreceivertesting

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var displayBatteryLevel: TextView;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Binding the text view with variable
        //to display the current battery level
        //of your phone
        displayBatteryLevel = findViewById(R.id.battery_level);
        //Registering broadcast receiver
        //to receive the changes in battery level
        registerReceiver(batteryBroadcastReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }
    private val batteryBroadcastReceiver: BroadcastReceiver = object:BroadcastReceiver()
    {
        //Called whenever there is a change in battery level
        @SuppressLint("SetTextI18n")
        override fun onReceive(context: Context?, intent: Intent?)
        {
            if(intent?.action == "android.intent.action.BATTERY_CHANGED")
            {
                //Fetching the current battery level
                val battery_level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                //Setting the battery level on display
                displayBatteryLevel.setText("$battery_level %");
                //Making a log statement
                //to show th battery level changed
                Log.d("TechVidvan", "Battery Level changed to : $battery_level");
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        //unregister the battery broadcast receiver
        //whenever the application is closed
        unregisterReceiver(batteryBroadcastReceiver);
    }

}